<?php

namespace App\Controller\Admin;

use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\RecettesRepository;
use App\Form\RecettesType;
use App\Entity\Recettes;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("/admin")
 */
class RecettesController extends AbstractController {

    /**
     * @Route("/recettes", name="admin.recettes.index")
     */
    public function index(RecettesRepository $recettesRepository): Response {
        $recettes = $recettesRepository->findAll();

        return $this->render('admin/recettes/index.html.twig', [
                    'recettes' => $recettes
        ]);
    }

    /**
     * @Route("/recettes/form", name="admin.recettes.form")
     * @Route("/recettes/update/{id}", name="admin.recettes.update")
     */
    public function form(Request $request, EntityManagerInterface $entityManager, RecettesRepository $recettesRepository, int $id = null): Response 
    {
        $type = RecettesType::class;
        
        $entity = $id ? new Recettes() : $recettesRepository->find($id);

        $form = $this->createForm($type, $entity);

        // handleRequest permet de récupérer de la saisie dans $_POST
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //dd($entity);
            $entityManager->persist($entity);
            $entityManager->flush();
            
            // message de confirmation
            $notification = 'La recette à bien été rajoutée.';
            $this->addFlash('notice', $notification);
            
            // redirection
            return $this->redirectToRoute('admin.recettes.index');
        }

        // envoyer le formulaire à la vue
        return $this->render('admin/recettes/form.html.twig',[
            'form' => $form->createView()
        ]);
    }

}
