<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class HomepageController extends AbstractController 
{
    /**
     * @Route("/hello/{name}", name="homepage.hello")
     */
    public function hello(string $name):Response
    {
        //dd($name);
        return $this->render('homepage/hello.html.twig', [
            'name' => $name
        ]);
    }


    /**
     * @Route("/", name="homepage.index")
     */
    public function index(Request $request):Response
    {
        $host = $request->headers->get('host');
        //dd($host);
        
        return $this->render('homepage/index.html.twig', [
            'hostFromController' => $host
        ]);
    }
}
