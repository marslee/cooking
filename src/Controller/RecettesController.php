<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\RecettesRepository;

class RecettesController extends AbstractController {

    /**
     * @Route("/recettes", name="recettes.index")
     */
    public function index(RecettesRepository $recettesRepository): Response 
    {
        $recettes = $recettesRepository->findAll();

        return $this->render('recettes/index.html.twig', [
                    'recettes' => $recettes
        ]);
    }

    /**
     * @Route("/recettes/{idRecette}", name="recettes.recette")
     */
    public function recette(int $idRecette, RecettesRepository $recettesRepository): Response 
    {
        
        $recette = $recettesRepository->find($idRecette);
        //dd($recette);

        return $this->render('recettes/recette.html.twig', [
                    'recette' => $recette
        ]);
    }

}
