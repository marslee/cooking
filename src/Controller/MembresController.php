<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\MembresRepository;

class MembresController extends AbstractController {

    /**
     * @Route("/membres", name="membres.index")
     */
    public function index(MembresRepository $membresRepository): Response
    {
        $membres = $membresRepository->findAll();

        return $this->render('membres/index.html.twig', [
                    'membres' => $membres
        ]);
    }

    /**
     * @Route("/membres/{idMembre}", name="membres.membre")
     */
    public function membre(int $idMembre, MembresRepository $membresRepository): Response
    {
        
        $membre = $membresRepository->find($idMembre);
        //dd($recette);

        return $this->render('membres/membre.html.twig', [
                    'membre' => $membre
        ]);
    }

}
