<?php

namespace App\Form;

use App\Entity\Recettes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class RecettesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => "Vous n'avez pas saisi le titre de la recette."
                    ]),
                    new Length([
                        'min' => 5,
                        'max' => 150,
                        'minMessage' => "Le titre de la recette doit comporter au minimum {{ limit }} caractères.",
                        'maxMessage' => "Le titre de la recette doit comporter au minimum {{ limit }} caractères."
                    ])
                ]
            ])
            ->add('chapo')
            ->add('img')
            ->add('preparation')
            ->add('ingredient')
            ->add('dateCrea')
            ->add('tempsCuisson')
            ->add('tempsPreparation')
            ->add('difficulte')
            ->add('prix')
            ->add('category')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Recettes::class,
        ]);
    }
}
